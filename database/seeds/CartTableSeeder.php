<?php

use Illuminate\Database\Seeder;
use App\Cart;
use App\User;

class CartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $count=User::all()->count();
       factory(Cart::class,$count)->create();
    }
}
