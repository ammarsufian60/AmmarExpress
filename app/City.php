<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable= [
        'name',
        'arabic_name',
        'country_id',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

}
