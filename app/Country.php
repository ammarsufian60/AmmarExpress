<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable= [
        'name',
        'alpha_code',
        'region',
        'name_arabic'
    ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
