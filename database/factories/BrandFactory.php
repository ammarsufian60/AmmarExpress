<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Brand;
use Faker\Generator as Faker;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'brand_name'=>$faker->name,
        'user_id'=>App\User::all()->random()->id,
        'category_id'=>App\Category::all()->random()->id,
        'status'=>1
    ];
});
