<?php

namespace App\Http\actions;

use App\Order;

class OrderCRUD
{
    public function create ($id)
    {
        $order = Order::create([
            'user_id'=>Auth::user()->id,
            'cart_id'=>$id,
            'total'=>Cart::find($id)->item->sum()
        ]);
    }
}
