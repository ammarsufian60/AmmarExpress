<?php

namespace App\Nova;

use App\Productdetails;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Product extends Resource
{
    public static $with = ['details'];
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Product';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'product_name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields (Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Product Name', 'product_name')->hideWhenUpdating()->rules(['required', 'string']),
            Text::make('Product Arabic Name', 'product_nameAR')->hideWhenUpdating()->rules(['required', 'string'])->hideFromIndex(),
            Text::make('quantity', 'product_quantity')->rules(['required', 'integer']),
            Text::make('price', 'product_price')->hideWhenUpdating()->rules(['required', 'string']),
            Number::make('Discount','discount'),
            Boolean::make('Status','status'),
            Textarea::make('description', 'product_description')->hideWhenUpdating()->rules(['required', 'string']),
            Textarea::make('arabic description', 'product_descriptionAR')->hideWhenUpdating()->rules(['required', 'string']),
            BelongsTo::make('brand')->hideWhenUpdating(),
            Text::make('Supplier Name',function(){
                return $this->brand->user->name;
            }),
            BelongsTo::make('Category','category',Category::class),
            HasMany::make('product variants', 'details', \App\Nova\Details::class)->hideWhenUpdating(),
            HasMany::make('Comments', 'comments', \App\Nova\Comment::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards (Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters (Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses (Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions (Request $request)
    {
        return [];
    }
}
