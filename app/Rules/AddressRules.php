<?php

namespace App\Rules;

use App\User;
use Illuminate\Contracts\Validation\Rule;

class AddressRules implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $count = User::find(Request()->user)->addresses->where('default_address',1)->count();
        if($count == 0) {
            return true;
        }else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'selected user have a default address';
    }
}
