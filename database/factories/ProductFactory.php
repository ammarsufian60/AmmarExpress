<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'brand_id'=>App\Brand::all()->random()->id,
        'category_id'=>App\Category::all()->random()->id,
        'product_name'=>$faker->name,
        'product_nameAR'=> $faker->name,
        'product_description'=>$faker->text,
        'product_descriptionAR'=>$faker->paragraph,
        'status'=>1,
        'product_price'=>$faker->randomDigit,
        'rating'=>$faker->randomDigit,
        'user_id'=>App\User::all()->random()->id,
        'product_quantity'=>$faker->randomDigit,
        'discount'=>$faker->randomDigit,

    ];
});
