<?php

namespace App\Console\Commands;

use App\Brand;
use Illuminate\Console\Command;

class BrandSlugCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slug:brands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add slug into brands table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
     Brand::all()->each(function($brand){
         $brand->slug=str_slug($brand->brand_name);
         $brand->save();
     });
    }
}
