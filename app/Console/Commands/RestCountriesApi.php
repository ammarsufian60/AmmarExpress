<?php

namespace App\Console\Commands;

require 'vendor/autoload.php';

use App\City;
use App\Country;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class RestCountriesApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'country:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get all country of world from restCountriesApi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client=new Client([
            'verified'=>false,
            'base_uri' => 'https://restcountries.eu/rest/v2/',
            'timeout'  => 1.0,
        ]);
        $response = $client->request('GET', 'all');
        collect(json_decode($response->getBody()->getContents()))->each(function($country){
           $Country=Country::updateOrCreate([
               'name' =>$country->name,
               'alpha_code' => $country->alpha2Code,
               'region' => $country->region,
               'name_arabic' => $country->nativeName,
           ]);
           if(strlen($country->name)>0) {
               City::updateOrCreate([
                   'name'=>$country->capital,
                   'arabic_name'=>$country->capital,
                   'country_id'=>$Country->id,
               ]);
           }
        });
    }
}
