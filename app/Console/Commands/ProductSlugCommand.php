<?php

namespace App\Console\Commands;

use App\Product;
use Illuminate\Console\Command;

class ProductSlugCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slug:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add slug into products table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Product::all()->each(function($product){
           $product->slug=str_slug($product->product_name);
           $product->save();
        });
    }
}
