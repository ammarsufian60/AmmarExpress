<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Address::class, function (Faker $faker) {
    return [

            'country_id' => $faker->randomDigit,
            'city_id' => $faker->randomDigit,
            'postal_code' => $faker->randomDigit,
            'user_id' => App\User::all()->random()->id,
            'default_address'=>$faker->boolean,

    ];
});
