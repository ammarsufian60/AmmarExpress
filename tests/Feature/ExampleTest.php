<?php

namespace Tests\Feature;

use App\Brand;
use App\Category;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use WithFaker,RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {

        $this->withoutExceptionHandling();
        $attributes=[
            'brand_id'=>1,// Brand::all()->random()->id,
            'category_id'=>1,//Category::all()->random()->id,
            'product_name'=>$this->faker->name,
            'product_nameAR'=> $this->faker->name,
            'product_description'=>$this->faker->text,
            'product_descriptionAR'=>$this->faker->paragraph,
            'status'=>1,
            'product_price'=>0,
            'rating'=>$this->faker->randomDigit,
            'user_id'=>1,//User::all()->random()->id,
            'product_quantity'=>$this->faker->randomDigit,
            'discount'=>$this->faker->randomDigit,
        ];
        $this->post('/api/products',$attributes);
        $this->assertDatabaseHas('products',$attributes);
    }
}
