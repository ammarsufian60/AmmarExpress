<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Brand;
use App\User;
use App\Category;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
       factory(Brand::class,500)->create();
    }
}
