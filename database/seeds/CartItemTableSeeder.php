<?php

use Illuminate\Database\Seeder;

class CartItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(\App\CartItem::class,100)->create();
    }
}
