<?php

namespace App\Http\Middleware;

use App\Productdetails;
use Closure;

class CheckProductQuantityOfCartItem
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $DataItem = Productdetails::where('product_id', '=', $request->product_id)->where('color', '=', $request->color)->where('size', '=', $request->size)->first();
        if ($DataItem->quantity >= $request->quantity)
        {
            return $next($request);
        }
        else {
            return 'the quantity available is' . $DataItem->quantity . ' that not enough';
        }
    }
}
