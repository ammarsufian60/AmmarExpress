<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cart;
use App\Model;
use App\Product;
use Faker\Generator as Faker;

$factory->define(\App\CartItem::class, function (Faker $faker) {
    return [
        'product_id' => Product::all()->random()->id,
        'cart_id' => Cart::all()->random()->id,
        'quantity' => $faker->numberBetween(1,100),
        'size' => $faker->randomDigit,
        'color' => $faker->ColorName,
    ];
});
