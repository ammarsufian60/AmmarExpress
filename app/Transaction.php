<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'from_id' ,
        'to_id' ,
        'cash'
    ];

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function from()
    {
        return $this->belongsTo(User::class);
    }

    public function to()
    {
        return $this->belongsTo(User::class);
    }
}
