<?php


namespace App\Http\actions;

use App\Product;
use Auth;
use App\CartItem;
use App\Http\Requests\CartItemRequest;

class CartItemCRUD
{
    public function create (CartItemRequest $request)
    {
       $product=Product::find($request->product_id);
       CartItem::create([
           'cart_id'=>Auth::user()->id,
           'size'=>$request->size,
           'color'=>$request->color,
           'quantity'=>$request->quantity,
           'product_id'=>$request->product_id,
           'price'=>$product->price,
           'discount'=>$product->discount,
           'total_price'=>($product->price - ($product->price * $product->discount ) ) * $request->quantity ,
       ]);
    }

    public function update (CartItemRequest $request, $id)
    {
       CartItem::where('id',$id)->update([
           'quantity'=>$request->quantity,
           'size'=>$request->size,
           'color'=>$request->color,
       ]);
    }
    public function destroy ($id)
    {
        $item = CartItem::find($id);
        $item->delete();
    }

    public function updateStatus(){

    }
}

